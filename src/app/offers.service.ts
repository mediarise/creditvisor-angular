import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';

@Injectable()
export class OffersService {

  constructor(private  http: HttpClient) {
  }

  url: string = '//console.creditstok.ru/api/offer';

  getOffers() {
    return this.http.get(this.url)
      .pipe(map(offers => {
        return offers;
      }));
  }
}


