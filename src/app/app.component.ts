import {Component} from '@angular/core';
import {OffersService} from './offers.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    OffersService
  ]
})
export class AppComponent {
  offers: any = [];

  constructor(private offersService: OffersService) {
  }

  ngOnInit() {
    this.offersService.getOffers().subscribe(offers => {
      this.offers = offers;
    });
  }

}
