import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {OfferComponent} from './offer/offer.component';
import {HttpClientModule} from '@angular/common/http';
import {HoverDirective} from './hover.directive';

@NgModule({
  declarations: [
    AppComponent,
    OfferComponent,
    HoverDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
